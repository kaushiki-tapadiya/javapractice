public class TryCatch {  
  
    public static void main(String[] args) {  
        try  
        {  
        int data=50/0;   
        }  
            //handling the exception  
        catch(ArithmeticException e)  
        {  
            System.out.println(e);  
        }  
        System.out.println("rest of the code");  
      
    
    try  
    {  
    int arr[]= {1,3,5,7};  
    System.out.println(arr[10]);  
    }  
        // handling the array exception  
    catch(ArrayIndexOutOfBoundsException e)  
    {  
        System.out.println(e);  
    }  
    System.out.println("rest of the code");  

  
    try{    
        int a[]=new int[5];    
        a[5]=30/0;    
       }    
       catch(ArithmeticException e)  
          {  
           System.out.println("Arithmetic Exception occurs");  
          }    
       catch(ArrayIndexOutOfBoundsException e)  
          {  
           System.out.println("ArrayIndexOutOfBounds Exception occurs");  
          }    
       catch(Exception e)  
          {  
           System.out.println("Parent Exception occurs");  
          }             
         
}  
}  
