
public class Primitive {

	public static void main(String[] args) {
		
	    byte b = 64;
		short s = 11;
		int i = 100;
		double pounds=5000d;
		float f=5.25f;
		
		long l = 500000 + 10 * (b+s+i);
		System.out.println(l);
		
		float minF = Float.MIN_VALUE;
		float maxF = Float.MAX_VALUE;
		System.out.println("Float ranges from "+minF+"  to "+maxF);


		double minD = Double.MIN_VALUE;
		double maxD = Double.MAX_VALUE;
		System.out.println("Double ranges from "+minD+"  to "+maxD);
		
		double PoundsinKilo = pounds*0.45359237d;
		System.out.println("Coversion is  "+PoundsinKilo);
		
		char c='K';
		char uniC='\u0011';
		System.out.println(c);
		System.out.println(uniC);
		
		boolean b1 = true;
		boolean b2 = false;
		if(b1!=b2)
			System.out.println("Not matched");
		
		
		
	}

}
