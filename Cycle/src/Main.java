import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {
	private static Map<Integer, Cycle> cycleObject = new HashMap<>();
    private static Set<Cycle> cyc = new HashSet<>();
    

	public static void main(String[] args) {
		
		Cycle temp=new Cycle(01, "MisterIndia", "Kalyan", 1235678);
		cycleObject.put(temp.getID(), temp);
		cyc.add(temp);
		
		temp=new Cycle(02, "TatyaVinchu", "Kalyan", 12356799);
		cycleObject.put(temp.getID(), temp);
		cyc.add(temp);
		
		temp=new Cycle(03, "Zingaat", "Bhavdan", 129935678);
		cycleObject.put(temp.getID(), temp);
		cyc.add(temp);
		
		temp=new Cycle(04, "Bazigar", "Bhavdan", 123535678);
		cycleObject.put(temp.getID(), temp);
		cyc.add(temp);
		
		temp=new Cycle(05, "ChachaChoudhary", "Pimpri", 1675);
		cycleObject.put(temp.getID(), temp);
		cyc.add(temp);
		
		temp=new Cycle(06, "ChhotaBheem", "Pimpri", 12678);
		cycleObject.put(temp.getID(), temp);
		cyc.add(temp);
		
		temp=new Cycle(07, "KachraSeth", "Kothrud", 123675678);
		cycleObject.put(temp.getID(), temp);
		cyc.add(temp);
		
		temp=new Cycle(8, "Gabbar", "Kothrud", 35678);
		cycleObject.put(temp.getID(), temp);
		cyc.add(temp);
		
		temp=new Cycle(9, "FrosenPrincess", "FC", 1235678);
		cycleObject.put(temp.getID(), temp);
		cyc.add(temp);
		
		temp=new Cycle(10, "Singham", "FC", 1235678);
		cycleObject.put(temp.getID(), temp);
		cyc.add(temp);
		
		System.out.println("Get called");
		
		
	
		
		
//		System.out.println("Cycle info.");
//        for(Cycle c : cyc) {
//            System.out.println(c.getID()+" --> "+c.getCycleName()+" --> "
//            		+ ""+c.getArea()+" --> "+c.getBarcode());
        }
		
		
		
		
		
		
		
		
//		 Scanner scanner = new Scanner(System.in);
//
//	        cycle.put(0, new Cycle(0, "ABC", "Kalyan", 1235678));
//	        cycle.put(1, new Cycle(0, "SBC", "Kalyan", 1235679));
//	        cycle.put(2, new Cycle(0, "DBC", "Viman nagar", 1295678));
//	        cycle.put(3, new Cycle(0, "FBC", "Viman nagar", 1205678));
//	        cycle.put(4, new Cycle(0, "DBC", "Andheri", 123567933));
//	        cycle.put(5, new Cycle(0, "CBC", "Andheri", 1235673));
//	        cycle.put(6, new Cycle(0, "KBC", "FC", 123567338));
//	        cycle.put(7, new Cycle(0, "LBC", "FC", 123533678));
//	        cycle.put(8, new Cycle(0, "PBC", "Kothrud", 133235678));
//	        cycle.put(9, new Cycle(0, "MBC", "Kothrud", 123335678));
//	        
//	        System.out.println(cycle);
	        

}
}
