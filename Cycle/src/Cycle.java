
import java.util.HashSet;
import java.util.Set;

public class Cycle {
    private final int ID;
    private final String area;
    private final String cycleName;
    private final long barcode;
    private final Set<Cycle> cycle;

    public Cycle(int ID, String cycleName,String area, long barcode) {
        this.ID = ID;
        this.cycleName = cycleName;
        this.area=area;
        this.barcode = barcode;
        this.cycle = new HashSet<>();
        
    }

	public int getID() {
		return ID;
	}

	public String getArea() {
		return area;
	}

	public String getCycleName() {
		return cycleName;
	}

	public long getBarcode() {
		return barcode;
	}
//
//	public boolean addMoon(Cycle cycleobj) {
//        return this.satellites.add(moon);
//    }

    public Set<Cycle> getCycle() {
        return new HashSet<>(this.cycle);
    }
    public void getInfo() {
    System.out.println("Cycle info.");
    for(Cycle c : cyc) {
        System.out.println(c.getID()+" --> "+c.getCycleName()+" --> "
        		+ ""+c.getArea()+" --> "+c.getBarcode());
    }
    }
    
}
