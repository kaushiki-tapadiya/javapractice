
public class IfElse {

	public static void main(String[] args) {
		int a=10, b=20, c=30;
		
		if (a>b&&b>c)
			System.out.println(a+ " is greatest");
		else if (b>a&&b>c)
			System.out.println(b+ " is greatest");
		else
			System.out.println(c+ " is greatest");
	}

}
