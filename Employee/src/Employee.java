
public class Employee {
	private String employeeName;
	private int employeeId;
	private double salary;
	private double pf;
	
	public Employee(){
		this("Default name",0000,0000);
    	System.out.println("Default constructor called");
    	
	}

	public Employee(String employeeName, int employeeId, double salary) {
		super();
		this.employeeName = employeeName;
		this.employeeId = employeeId;
		this.salary = salary;
		
	}
	
	
	public void calculate(double allowances) {
		this.pf = 0.08*salary;
		this.salary = salary-pf+allowances;
		
		//System.out.println("Employee name : "+this.employeeName+"\nEmployee Id : "+this.employeeId+"\nSalary : "+this.salary);
	}
	
	public void display()
	{
	
	System.out.println("Employee name : "+getEmployeeName()+"\nEmployee Id : "+getEmployeeId()+"\nSalary : "+getSalary());
	System.out.println("PF : " +getPf());
	}
	

	public double getPf() {
		return pf;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public double getSalary() {
		return salary;
	}	
}
