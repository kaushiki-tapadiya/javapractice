
public class Account {
	
	private long accNo;
	private long phoneNo;
	private double bal;
	private String name;
	private String email;
	
	public Account(){
        	System.out.println("Default constructor called");
		this(12345, 154590, 5656.0 , "GHAJXgv", "hxuhjw@email.com")	
	}
	
	public Account(long accNo, long phoneNo, double bal, String name, String email) {
		super();
		this.accNo = accNo;
		this.phoneNo = phoneNo;
		this.bal = bal;
		this.name = name;
		this.email = email;
	}
	public void deposit(double amount)
	{
		this.bal += amount;
		System.out.println("Amount deposited "+amount+ " Makes balance "+this.bal);
	}
	public void withdraw(double amount)
	{
		if (bal>=amount)
		System.out.println("After withdrawing "+amount+ " Available balance is " +(this.bal-amount));
		else
			System.out.println("Insufficient Balance");
			
	}


   
	public long getAccNo() {
		return accNo;
	}
	public void setAccNo(long accNo) {
		this.accNo = accNo;
	}
	public long getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(long phoneNo) {
		this.phoneNo = phoneNo;
	}
	public double getBal() {
		return bal;
	}
	public void setBal(double bal) {
		this.bal = bal;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	

}
