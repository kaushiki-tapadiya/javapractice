package calci;

public class SimpleCalculator {
	
	private double firstno;
	private double secondno;
	
	public double getFirstno() {
		return firstno;
	}
	public double getSecondno() {
		return secondno;
	}
	public void setFirstno(double firstno) {
		this.firstno = firstno;
	}
	public void setSecondno(double secondno) {
		this.secondno = secondno;
	}
	public double getAdditionResult() {
		return(this.firstno+this.secondno);
	}
	public double getSubtractionResult() {
		return(this.firstno-this.secondno);
	}
	public double getMultiplicationResult() {
		return(this.firstno*this.secondno);
	}
	public double getDivisionResult() {
		if(this.secondno==0)
			return(0);
		else
		return(this.firstno/this.secondno);
	}
	
	

}
