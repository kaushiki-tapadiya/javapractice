package calci;

public class Main {

	public static void main(String[] args) {
		SimpleCalculator calculator=new SimpleCalculator();
		calculator.setFirstno(5.0);
		calculator.setSecondno(4);
		System.out.println("add" + calculator.getAdditionResult());
		System.out.println("sub" + calculator.getSubtractionResult());
		System.out.println("div" + calculator.getDivisionResult());
		calculator.setFirstno(5.25);
		calculator.setSecondno(0);
		System.out.println("mul" + calculator.getMultiplicationResult());
		System.out.println("div" + calculator.getDivisionResult());
	}

}
