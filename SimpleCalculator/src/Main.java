
public class Main {

	public static void main(String[] args) {

		Account acc = new Account();
		Account dpAcc =  new Account(1233134415,124567890,800.0,"Deepu","Deepu@me.com");
		
		dpAcc.withdraw(500.0);
		dpAcc.deposit(500.899);
		dpAcc.withdraw(0.899);
		
		System.out.println(dpAcc.getEmail());
		dpAcc.getEmail();
		
	}

}
