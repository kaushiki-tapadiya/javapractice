
@FunctionalInterface //Since interface have exactly one abs method
interface Cab{
	void book(); //public abstract bd
	//void bookanother(); //not valid
}

@FunctionalInterface
interface AnotherCab{
	void bookcab(String src, String desti);	
}

@FunctionalInterface
interface ReturnCab{
	double returncab(String src, String desti);
}

//class UberCab implements Cab{
//	public void book() {
//		System.out.println("Booked");
//	}
//	
//}

public class Lambda {

	public static void main(String[] args) {

//		Cab cab =  new UberCab(); //Polymorphic ststement
//		cab.book();

		//Ano inner type class
//		Cab cab = new Cab() {
//			
//			@Override
//			public void book() {
//				System.out.println("Booked");
//			}
//		};
//		cab.book();
		
		//Using lambda (works only with fun interfaces)
		Cab cab=() ->{System.out.println("Booked"); //lambda with no inputs and return types
     	};cab.book();
		
     	//Using parameters
		AnotherCab ac = (src, desti)->{
			System.out.println("Booked with AnotherCab from "+src+" to "+desti);
		};
		ac.bookcab("Viman Nagar", "FC road");

		//Using return method
		ReturnCab rc = (src,desti)->{
			System.out.println("Booked with return method");
			return 810;
		};
		double fare=rc.returncab("Viman Nagar", "FC road");
		System.out.println("Fares are " +fare);
	}


}
