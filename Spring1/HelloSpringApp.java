

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {

	public static void main (String[] args) {

		// load the spring configuration file
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");//created spring container
				
		// retrieve bean from spring container
		Coach theCoach = context.getBean("myCoach", Coach.class);
		//Coach theCoach = context.getBean("BeanID", interface);
		
		// call methods on the bean
		System.out.println(theCoach.getDailyWorkout());
		
		// close the context
		context.close();
	}

}







