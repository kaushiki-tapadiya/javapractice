package kaushiki;


	public class Cycle {
		
		private int id;
		private String name;
		private long barcode;
		private String place;
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public long getBarcode() {
			return barcode;
		}
		public void setBarcode(long barcode) {
			this.barcode = barcode;
		}
		public String getPlace() {
			return place;
		}
		public void setPlace(String place) {
			this.place = place;
		}
		@Override
		public String toString() {
			return "Cycle [id=" + id + ", name=" + name + ", barcode=" + barcode + ", place=" + place + "]";
		}

	
}
