package kaushiki;

import java.util.*;
class Sample
{
	
	public static void main(String args[])
	{
		Cycle c[]=new Cycle[10];
		while(true)
		{	
			Scanner sc=new Scanner(System.in);
			
			System.out.println("1. Add cycle");
			System.out.println("2. All park cycle");
			System.out.println("3. Move Cycle");
			System.out.println("4. Remove cycle");
			System.out.println("5. Change name cycle");
			System.out.println("6. exit");
			int n=sc.nextInt();
			switch(n)
			{
				case 1: Add(c);
						break;
				case 2: Show(c);
						break;
				case 3: Move(c);
						break;
				case 4: Remove(c);
						break;
				case 5: Change(c);
						break;
				case 6:	System.exit(0);
			}
		}
	}
	static void Add(Cycle c[])
	{
		Scanner sc=new Scanner(System.in);
		for(int i=0;i<c.length;i++)
		{
			int id=sc.nextInt();sc.nextLine();
			String name=sc.nextLine();
			Long bar=sc.nextLong();sc.nextLine();
			String place=sc.nextLine();
			c[i]=new Cycle(id,name,bar,place);
		}
	}
	
	 static void Show(Cycle c[])
	{
		
		for(int i=0;i<c.length;i++)
		{
			System.out.println(c[i].getid()+"|"+c[i].getname()+"|"+c[i].getbarcode()+"|"+c[i].getplace());
		}
	}
	
	 static void Move(Cycle c[])
	{
		Scanner sc=new Scanner(System.in);
		String src=sc.nextLine();
		String updatedsrc=sc.nextLine();
		int flag=0;
		int i=0;
		for(i=0;i<c.length;i++)
		{
			if(src.equals(c[i].place))
			{
				c[i].setplace(updatedsrc);
				flag=1;
				//c1=i;
				break;
			}
		}
		if(flag==1)
			System.out.println(c[i].getid()+"|"+c[i].getname()+"|"+c[i].getbarcode()+"|"+c[i].getplace());
		else
			System.out.println("No cycle found");
	}	
	
	 static void Remove(Cycle c[])
	{
		Scanner sc=new Scanner(System.in);
		String plc=sc.nextLine();
		int flag=0,i=0;
		for(i=0;i<c.length;i++)
		{
			if(plc.equals(c[i].place))
			{
				flag=1;
				//c1=i;
				break;
			}	
		}
		if(flag==1)
		{
			System.out.println(c[i].getid()+"|"+c[i].getname()+"|"+c[i].getbarcode()+"|"+c[i].getplace());
			
			for(int j=i+1;j<c.length;j++)
			{
				c[j-1]=c[j];
			}
		}
		else
			System.out.println("No cycle found");
	}
	
	 static void Change(Cycle c[])
	{
		Scanner sc=new Scanner(System.in);
		String name=sc.nextLine();
		String updatedname=sc.nextLine();
		int flag=0,i=0;
		for(i=0;i<c.length;i++)
		{
			if(name.equals(c[i].name))
			{
				c[i].setname(updatedname);
				flag=1;
				//c1=i;
				break;
			}
		}
		if(flag==1)
			System.out.println(c[i].getid()+"|"+c[i].getname()+"|"+c[i].getbarcode()+"|"+c[i].getplace());
		else
			System.out.println("No cycle found");
	}

}
class Cycle
{
	int id;
	String name;
	long barcodeno;
	String place;
	
	
	Cycle(int id, String name, long barcodeno,String place)
	{
		this.id=id;
		this.name=name;
		this.barcodeno=barcodeno;
		this.place=place;
	}
	
	public void setplace(String place)
	{
		this.place=place;
	}

	public void setname(String name)
	{
		this.name=name;
	}

	public String getplace()
	{
		return this.place;
	}
	
	public int getid()
	{
		return this.id;
	}
	
	public String getname()
	{
		return this.name;
	}
	
	public long getbarcode()
	{
		return this.barcodeno;
	}
}