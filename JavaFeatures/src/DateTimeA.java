import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;

public class DateTimeA {

	public static void main(String[] args) {
		LocalDate d=LocalDate.now();
		System.out.println(d);
		
		LocalDate e=LocalDate.of(1999, 4, 20);
		System.out.println(e);
		
		LocalDate d3=LocalDate.of(1999, Month.APRIL, 20);
		System.out.println(d3);
		
//		LocalDate d4=LocalDate.of(2020, 02, 30);
//		System.out.println(d4); //Not a valid date
		
		LocalTime t = LocalTime.now();
		System.out.println(t);
		
		LocalTime t1 = LocalTime.of(10, 30, 01);
		System.out.println(t1);

//		LocalTime t1 = LocalTime.of(10, 300, 01);
//		System.out.println(t1);
		
		LocalTime tt=LocalTime.now(ZoneId.of("Africa/Nairobi"));
		System.out.println(tt);
		
		LocalDateTime tq = LocalDateTime.now();
		System.out.println(tq);
		
		Instant i = Instant.now();
		System.out.println(i);      //GMT
		
//		//Fetch zone id
//		for (String s : ZoneId.getAvailableZoneIds())
//		{
//			System.out.println(s);
//		}
		
		
	}

}
