import java.io.PrintWriter;
import java.io.*;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class Welcome extends HttpServlet{
	
	private String message;
	
	public void init() throws ServletException{
		//Do initializations
		message = "Welcome to my project";
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
			//Set response content type
			response.setContentType("text/html");
			
			//Actual login goes here
			PrintWriter out = response.getWriter();
			out.print("<h1>" +message+ "</h1>");
		}
		public void destroy() {}
	}


