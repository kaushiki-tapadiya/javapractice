
public class Main {
    public static void main(String[] args) {
        ITelephone myPhone;
        myPhone = new Landline(123456);
        myPhone.powerOn();
        myPhone.callPhone(123456);
        myPhone.answer();

        myPhone = new Mobile(24565);
        myPhone.powerOn();
        myPhone.callPhone(24565);
        myPhone.answer();

    }

}
